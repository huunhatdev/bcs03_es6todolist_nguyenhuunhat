document.querySelector("#addItem").addEventListener("click", () => {
  addTask();
});

document.querySelector("#newTask").addEventListener("keyup", (e) => {
  if (e.key === "Enter" || e.keyCode === 13) {
    addTask();
  }
});

document.querySelector("#one").addEventListener("click", () => {
  document.querySelector("#one").classList.toggle("fillter");
  fillerComplete();
});
document.querySelector("#two").addEventListener("click", () => {
  sortA_Z();
});
document.querySelector("#three").addEventListener("click", () => {
  sortZ_A();
});
document.querySelector("#all").addEventListener("click", () => {
  sortTime();
});
