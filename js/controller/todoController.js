const ListTask = [];
let tempListTask = [];
const Todo_LocalStorage = "Todo_LocalStorage";

const saveStorage = () => {
  localStorage.setItem(Todo_LocalStorage, JSON.stringify(ListTask));
};

const renderTask = (arr) => {
  let contentComplete = "";
  let contentTodo = "";

  arr.forEach((e, i) => {
    if (e.isComplete) {
      contentComplete += `<li>
        <span>${e.taskName}</span>
        <div class="buttons">
          <button class="fa fa-trash-alt buttons remove " onclick="btnDelete(${i})"></button>
          <button class="fas fa-times-circle complete" onclick="btnUnComplete(${i})"></button>
        </div>
      </li>`;
    } else {
      contentTodo += `<li>
        <span>${e.taskName}</span>
        <div class="buttons">
          <button class="fa fa-trash-alt buttons remove" onclick="btnDelete(${i})"></button>
          <button class="fa fa-check complete" onclick="btnComplete(${i})")></button>
        </div>
      </li>`;
    }
  });

  document.querySelector("#todo").innerHTML = contentTodo;
  document.querySelector("#completed").innerHTML = contentComplete;
};

let jsonListTask = localStorage.getItem(Todo_LocalStorage);
if (jsonListTask) {
  pare = JSON.parse(jsonListTask);
  pare.forEach((e) => {
    ListTask.push(e);
  });
  tempListTask = [...ListTask];
  renderTask(ListTask);
}

const addTask = () => {
  let task = document.querySelector("#newTask").value.trim();
  if (!task) {
    return;
  }

  let newTask = {
    taskName: task,
    isComplete: false,
    time: Math.floor(new Date().getTime() / 1000),
  };
  ListTask.push(newTask);
  tempListTask = [...ListTask];
  saveStorage();
  renderTask(ListTask);
  document.querySelector("#newTask").value = "";
};

const findI = (taskName) => {
  return ListTask.findIndex((e) => {
    return e.taskName == taskName;
  });
};

const btnComplete = (i) => {
  let taskName = tempListTask[i].taskName;
  ListTask[findI(taskName)].isComplete = true;
  tempListTask = [...ListTask];
  saveStorage();
  renderTask(ListTask);
};

const btnUnComplete = (i) => {
  let taskName = tempListTask[i].taskName;
  ListTask[findI(taskName)].isComplete = false;
  tempListTask = [...ListTask];
  saveStorage();
  renderTask(tempListTask);
};

const btnDelete = (i) => {
  let taskName = tempListTask[i].taskName;
  ListTask.splice(findI(taskName), 1);
  tempListTask = [...ListTask];
  saveStorage();
  renderTask(ListTask);
};

const sortA_Z = () => {
  let arr = tempListTask.sort((a, b) => {
    if (a.taskName > b.taskName) return 1;
    if (a.taskName < b.taskName) return -1;
    return 0;
  });
  renderTask(arr);
};

const sortZ_A = () => {
  let arr = tempListTask.sort((a, b) => {
    if (a.taskName > b.taskName) return -1;
    if (a.taskName < b.taskName) return 1;
    return 0;
  });
  renderTask(arr);
};

const fillerComplete = () => {
  let arr = tempListTask.filter((e) => {
    return e.isComplete === true;
  });
  renderTask(arr);
};

const sortTime = () => {
  let arr = ListTask.sort((a, b) => {
    if (a.time > b.time) return 1;
    if (a.time < b.time) return -1;
    return 0;
  });
  renderTask(arr);
};
